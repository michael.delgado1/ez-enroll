package finalphase.application;

import finalphase.domain.Searchable;
import finalphase.domain.courseModule.Course;
import finalphase.domain.userModule.Student;


public class StudentManager {
	
	public StudentManager() {}
	
	public double calculatePassingRate(Course course, String max, String min) {
		isValid(course);
		return course.getPassingRate(max, min);
	}
	
	public boolean enroll(Course course, Student student) {
		isValid(course);
		isValid(student);
		if(isNotFull(course)) {
			course.addStudent(student);
			student.addCourse(course);
			student.setCredits(student.getCredits()+course.getCredits());
			return true;
		}
		return false;
	}
	
	public boolean drop(Course course, Student student) {
		isValid(course);
		isValid(student);
		course.removeStudent(student);
		student.removeCourse(course);
		student.setCredits(student.getCredits()-course.getCredits());
		return true;
	}

	public boolean addComment(Course course, Student student, String comment) {
		isValid(course);
		isValid(student);
		if(course.getStudentsList().contains(student)) {
			course.addComment(comment);
			return true;
		}
		return false;
	}
	
	private static void isValid(Searchable searchable) throws IllegalArgumentException {
		if(searchable == null) throw new IllegalArgumentException("Searchable is null");
	}
	
	private static boolean isNotFull(Course course) {
		isValid(course);
		return course.getNumStudents() < course.getMaxCapacity();
	}
}
