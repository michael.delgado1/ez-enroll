package finalphase.application;

import java.util.ArrayList;

import finalphase.domain.Searchable;
import finalphase.domain.courseModule.Course;
import finalphase.domain.userModule.Professor;
import finalphase.domain.userModule.Student;
import finalphase.infrastructure.Pair;

public class SearchableFactory {

	public Searchable CourseNewInstance(int id, String name, String title, int code, int section, String academicYears, int credits,
			int maxCapacity, String location, Professor professor, String description) {
		return new Course(id, name, title, code, section, academicYears, credits,
				maxCapacity, location, professor, description);
	}

	public Searchable StudentNewInstance(int id, String name, int studentNumber, int pin, 
			int tuitionBill, int scholarshipAid, int periodOfPaymentPlan, int maxCredits, 
			String graduate, ArrayList<Pair<Course, String>> grades) {
		return new Student(id, name, studentNumber, pin, tuitionBill, scholarshipAid, periodOfPaymentPlan,
				maxCredits, graduate, grades);
	}

	public Searchable ProfessorNewInstance(int id, String name, String username, String password, ArrayList<Course> schedule) {
		return new Professor(id, name, username, password, schedule);
	}
}