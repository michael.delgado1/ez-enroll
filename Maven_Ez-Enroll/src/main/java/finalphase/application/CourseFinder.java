package finalphase.application;
import java.util.LinkedList;

import finalphase.domain.Searchable;
import finalphase.domain.courseModule.Course;
import finalphase.domain.userModule.Professor;

public class CourseFinder {

	public CourseFinder() {}
	
	public LinkedList<Course> findCourse(String name, String title, int code, int section, String academicYears, 
			Professor professor, String maxGrade, String minGrade, double passingRate, Searchable[] dB) {
		LinkedList<Course> result = new LinkedList<Course>();
		result = this.findCourseByName(name, result, dB);
		result = this.findCourseByTitle(title, result, dB);
		result = this.findCourseByCode(code, result, dB);
		result = this.findCourseBySection(section, result, dB);
		result = this.findCourseByAcademicYears(academicYears, result, dB);
		result = this.findCourseByProfessor(professor, result, dB);
		result = this.findCourseByPassingRate(maxGrade, minGrade, passingRate, result, dB);
		return result;
	}
	
	public LinkedList<Course> findCourseByName(String name, LinkedList<Course> courseList, Searchable[] dB) {
		if(name == null) {
			for (Searchable course : dB)
				if (course instanceof Course) courseList.add((Course) course);
		}
		else {
			for (Searchable course : dB)
				if (course instanceof Course && course.getName() == name) courseList.add((Course) course);
		}
		return courseList;
	}
	
	public LinkedList<Course> findCourseByTitle(String title, LinkedList<Course> courseList, Searchable[] dB) {
		if(title == null) return courseList;
		else {
			LinkedList<Course> result = new LinkedList<Course>();
			for (Course course : courseList)
				if (course.getTitle() == title) result.add(course);
			return result;
		}
	}

	public LinkedList<Course> findCourseByCode(int code, LinkedList<Course> courseList, Searchable[] dB) {
		if(code == -1) return courseList;
		LinkedList<Course> result = new LinkedList<Course>();
		for (Course course : courseList)
			if (course.getCode() == code) result.add(course);
		return result;
	}
	
	public LinkedList<Course> findCourseBySection(int section, LinkedList<Course> courseList, Searchable[] dB) {
		if(section == -1) return courseList;
		LinkedList<Course> result = new LinkedList<Course>();
		for (Course course : courseList)
			if (course.getSection() == section) result.add(course);
		return result;
	}
	
	public LinkedList<Course> findCourseByAcademicYears(String academicYears, LinkedList<Course> courseList, Searchable[] dB) {
		if(academicYears == null) return courseList;
		else {
			LinkedList<Course> result = new LinkedList<Course>();
			for (Course course : courseList)
				if (course.getAcademicYears() == academicYears) result.add(course);
			return result;
		}
	}
	
	public LinkedList<Course> findCourseByProfessor(Professor professor, LinkedList<Course> courseList, Searchable[] dB) {
		if(professor == null) return courseList;
		else {
			LinkedList<Course> result = new LinkedList<Course>();
			for (Course course : courseList)
				if (course.getProfessor() == professor) result.add(course);
			return result;
		}
	}	
	
	public LinkedList<Course> findCourseByPassingRate(String maxGrade, String minGrade, double passingRate, 
			LinkedList<Course> courseList, Searchable[] dB) {
		if(maxGrade == null || minGrade == null || passingRate == -1) return courseList;
		else {
			LinkedList<Course> result = new LinkedList<Course>();
			for (Course course : courseList)
				if (course.getPassingRate(maxGrade, minGrade) == passingRate) result.add(course);
			return result;
		}
	}	
}