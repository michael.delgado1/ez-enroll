package finalphase.infrastructure;

public class Pair<Searchable, Grade> {
	
	private Searchable s;
	private Grade g;
	
	public Pair(Searchable s) {
		this.s = s;
		this.g = null;
	}
	public Pair(Searchable s, Grade g) {
		this.s = s;
		this.g = g;
	}
	public Searchable getSearchable() {
		return s;
	}
	public Grade getGrade() {
		return g;
	}
	public void setSearchable(Searchable s) {
		this.s = s;
	}
	public void setGrade(Grade g) {
		this.g = g;
	}
}