//package finalphase.infrastructure;
//
//import static org.junit.Assert.*;
//
//import java.util.ArrayList;
//
//import org.junit.Test;
//
//import finalphase.application.CourseFinder;
//import finalphase.application.SearchableFactory;
//import finalphase.application.StudentManager;
//import finalphase.domain.Searchable;
//import finalphase.domain.courseModule.Course;
//import finalphase.domain.userModule.Professor;
//import finalphase.domain.userModule.Student;
//
//public class Tests {
//
//	StudentManager manager = new StudentManager();
//	CourseFinder finder = new CourseFinder();
//	SearchableFactory factory = new SearchableFactory(); 
//	Student s1 = (Student) factory.StudentNewInstance(1, "Michael Delgado", 802131851, 0546, 1300, 0, 5, 21, "False", null);
//	Student s2 = (Student) factory.StudentNewInstance(2, "Mia Medina", 801197564, 5813, 300, 300, 0, 18, "False", null);
//	Student s3 = (Student) factory.StudentNewInstance(3, "Christian Lopez", 801149465, 4002, 2000, 300, 3, 18, "True", null);
//	Professor p1 = (Professor) factory.ProfessorNewInstance(4, "Marko Schutz", "marko.schutz", "domain101", null);
//	Professor p2 = (Professor) factory.ProfessorNewInstance(5, "Paulo Coehlo", "paulo.coehlo", "cd4s164we", null);
//	Course c1 = (Course) factory.CourseNewInstance(6, "Software Design", "INSO",  4116, 040, "2018-2019", 3, 30, 
//			"S-309", p1, "This is a software course.");
//	Course c2 = (Course) factory.CourseNewInstance(7, "Software Design", "INSO",  4116, 040, "2019-2020", 3, 30, 
//			"S-309", p1, "This is a software course.");
//	Course c3 = (Course) factory.CourseNewInstance(8, "Game Design", "GAME",  4046, 050, "2020-2021", 3, 20, 
//			"S-309", p2, "This is a software course.");
//	
//	Searchable[] dB = {s1, s2, s3, p1, c1, c2};
//	
//	
//	@Test
//	public void testStudentManager() {
//		ArrayList<Course> p1Schedule = new ArrayList<Course>();
//		p1Schedule.add(c1);
//		p1Schedule.add(c2);
//		p1.setSchedule(p1Schedule);
//		
//		ArrayList<Course> p2Schedule = new ArrayList<Course>();
//		p2Schedule.add(c2);
//		p2.setSchedule(p2Schedule);
//		
//		ArrayList<Student> c1StudentsList = new ArrayList<Student>();
//		c1StudentsList.add(s1);
//		c1.setStudentsList(c1StudentsList);
//		
//		ArrayList<Student> c2StudentsList = new ArrayList<Student>();
//		c2StudentsList.add(s1);
//		c2StudentsList.add(s2);
//		c2.setStudentsList(c2StudentsList);
//		
//		ArrayList<Pair<Course, String>> s1Grades = new ArrayList<Pair<Course, String>>();
//		s1Grades.add(new Pair<Course,String>(c1, "D"));
//		s1Grades.add(new Pair<Course,String>(c2, "A"));
//		s1.setGrades(s1Grades);
//		
//		ArrayList<Pair<Course, String>> s2Grades = new ArrayList<Pair<Course, String>>();
//		s2Grades.add(new Pair<Course,String>(c2, "C"));
//		s2.setGrades(s2Grades);
//		
//		assertTrue("manager.enrolls: Yields false incorrectly", manager.enroll(c3, s1) == true);
//		assertTrue("manager.enrolls: Yields false incorrectly", c3.getStudentsList().contains(s1));
//		assertTrue("manager.enrolls: Yields false incorrectly", s1.getSchedule().contains(c3));
//		
//		assertTrue("manager.drop: Yields false incorrectly", manager.drop(c3, s1) == true);
//		assertFalse("manager.drop: Yields true incorrectly", c3.getStudentsList().contains(s1));
//		assertFalse("manager.drop: Yields true incorrectly", s1.getSchedule().contains(c3));
//		
//		assertTrue("manager.calculatePassingRate: Yields false incorrectly", manager.calculatePassingRate(c2, "A", "C") == 1);
//		
//		assertTrue("manager.addComment: Yields false incorrectly", manager.addComment(c1, s1, "It's really hard!"));
//		assertFalse("manager.addComment: Yields true incorrectly", c1.getCommentSection().isEmpty());
//		
//		//test CourseFinder
//		assertTrue("finder.findCourse: Yields false incorrectly", finder.findCourse("Software Design", 
//				"INSO",  4116, 040, "2018-2019", p1, 
//				"A", "D", 1, dB).getFirst().getId() == c1.getId());
//	}
//	
//	@Test
//	public void testProfessorModifyCourseDescription() {
//		assertTrue("p1.modifyCourseDescription: Yields false incorrectly", 
//				p1.modifyCourseDescription(c1, "Hi..."));
//		assertFalse("p1.modifyCourseDescription: Yields true incorrectly", c1.getDescription() == "This is a software course.");
//	}
//}
