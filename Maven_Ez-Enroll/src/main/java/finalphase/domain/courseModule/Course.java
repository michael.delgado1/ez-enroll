package finalphase.domain.courseModule;

import java.util.ArrayList;
import java.util.LinkedList;

import finalphase.domain.Searchable;
import finalphase.domain.userModule.Professor;
import finalphase.domain.userModule.Student;
import finalphase.infrastructure.Pair;

public class Course implements Searchable {
	
	private int id;			//Unique id provided by the UPRM database	
	private String name;	//Descriptive name					EXAMPLE: Software Design
	
	private String title;	//Department's initials				EXAMPLE: INSO
	private int code;		//Codification in the UPRM database	EXAMPLE: 4116
	private int section;	//Number of the exact class			EXAMPLE: 040
	private String academicYears; //Year when it was taught		EXAMPLE: 2019-2020
	private int credits;	//Amount of credits represented		EXAMPLE: 3
	private int maxCapacity; //Students enrolled limit			EXAMPLE: 20
	private String location;	//Place (mostly classrooms or laboratories) where the course is given
	private Searchable professor;	//Instructor of course
	private String description;	//Paragraph created by professor to explain the course  purpose and goals
	
	private ArrayList<Student> studentsList;	//List of enrolled students and their grades
	private LinkedList<String> commentSection;
	
	public Course() {
		this.studentsList = new ArrayList<Student>();
		this.commentSection = new LinkedList<String>();
	}
	public Course(int id, String name, String title, int code, int section, String academicYears, int credits,
			int maxCapacity, String location, Professor professor, String description) {
		super();
		this.id = id;
		this.name = name;
		
		this.title = title;
		this.code = code;
		this.section = section;
		this.academicYears = academicYears;
		this.credits = credits;
		this.maxCapacity = maxCapacity;
		this.location = location;
		this.professor = professor;
		this.description = description;
				
		//Fields assumed to be empty in the creation of a course
		this.studentsList = new ArrayList<Student>();
		this.commentSection = new LinkedList<String>();
	}
	
	
	@Override
	public int getId() {
		return id;
	}
	@Override
	public String getName() {
		return name;
	}

	public String getTitle() {
		return title;
	}
	public int getCode() {
		return code;
	}
	public int getSection() {
		return section;
	}
	public String getAcademicYears() {
		return academicYears;
	}
	public int getCredits() {
		return credits;
	}
	public int getMaxCapacity() {
		return maxCapacity;
	}
	public String getLocation() {
		return location;
	}
	public Searchable getProfessor() {
		return professor;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public ArrayList<Student> getStudentsList() {
		return studentsList;
	}

	public LinkedList<String> getCommentSection() {
		return commentSection;
	}
	
	public boolean addComment(String comment) {
		return this.getCommentSection().add(comment);
	}
	
	public boolean addStudent(Student student) {
		return this.getStudentsList().add(student);
	}
	public boolean removeStudent(Student student) {
		return this.getStudentsList().remove(student);
	}
	public int getNumStudents() {
		return this.getStudentsList().size();
	}
	
	public double getPassingRate(String max, String min) {
		double result = 0;
		int counter = 0; //adjustment for students with null grades
		for(Student s : this.getStudentsList()) {
			String tempGrade =  findGrade(s, this);
			if(tempGrade == null) {counter++;}
			else {
				if(max.compareTo(tempGrade) <= 0 && min.compareTo(tempGrade) >= 0) {
					result++;
				}
			}
		}
		return result/(getNumStudents()-counter);
	}
	
	private static String findGrade(Student student, Course course) {
		if(student != null && course != null) {
			for(Pair<Course,String> p : student.getGrades())
				if(p.getSearchable().getId() == course.getId()) return p.getGrade();
		}
		return null;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((academicYears == null) ? 0 : academicYears.hashCode());
		result = prime * result + ((studentsList == null) ? 0 : studentsList.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + code;
		result = prime * result + credits;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + maxCapacity;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((professor == null) ? 0 : professor.hashCode());
		result = prime * result + section;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Course other = (Course) obj;
		if (academicYears == null) {
			if (other.academicYears != null)
				return false;
		} else if (!academicYears.equals(other.academicYears))
			return false;
		if (studentsList == null) {
			if (other.studentsList != null)
				return false;
		} else if (!studentsList.equals(other.studentsList))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (code != other.code)
			return false;
		if (credits != other.credits)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (maxCapacity != other.maxCapacity)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (professor == null) {
			if (other.professor != null)
				return false;
		} else if (!professor.equals(other.professor))
			return false;
		if (section != other.section)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Course [id=" + id + ", name=" + name + ", title=" + title  + ", code=" + code 
				+ ", section=" + section + ", academicYears=" + academicYears + ", maxCapacity=" + maxCapacity 
				+ ", credits=" + credits + ", professor=" + professor + ", description=" + description 
				+ ", studentsList=" + studentsList + ", location=" + location + "]";
	}
	
	//just for testing
	public void setStudentsList(ArrayList<Student> studentsList) {this.studentsList = studentsList;}
}