package finalphase.domain.userModule;

import java.util.ArrayList;

import finalphase.domain.Searchable;
import finalphase.domain.courseModule.Course;
import finalphase.infrastructure.Pair;

public class Student implements Searchable {

	
	private int id;	//Unique id provided by the UPRM database
	private String name;	//Descriptive name
	
	private int studentNumber;
	private int pin;
	
	private int tuitionBill;	//How much money the student have to pay
	private int scholarshipAid;	//How much money the student receive as aid to pay the tuitionBill
	private int periodOfPaymentPlan; //Periods of payment ranging from 1 to 5 for the student to pay the tuitionBill 
	private int maxCredits;	//Max credits the student is allowed to enroll
	private String graduate;	//Determines if the student is a graduate one or not
	private ArrayList<Pair<Course, String>> grades;//List of courses and strings (the strings will be null for professors and grades for students)
	
	private int credits;	//Actual amount of credits enrolled
	private ArrayList<Course> schedule;	//Courses enrolled
	
	public Student() {
		this.credits = 0;
		this.schedule = new ArrayList<Course>();
	}
	
	public Student(int id, String name, int studentNumber, int pin, int tuitionBill, int scholarshipAid, int periodOfPaymentPlan,
			int maxCredits, String graduate, ArrayList<Pair<Course, String>> grades) {
		
		this.id = id;
		this.name = name;
		
		this.studentNumber = studentNumber;
		this.pin = pin;
		
		this.tuitionBill = tuitionBill-scholarshipAid;
		this.scholarshipAid = scholarshipAid;
		this.periodOfPaymentPlan = periodOfPaymentPlan;
		this.maxCredits = maxCredits;
		this.graduate = graduate;
		this.grades = new ArrayList<Pair<Course, String>>();
		
		this.credits = 0;
		this.schedule = new ArrayList<Course>();
	}

	
	@Override
	public int getId() {
		return id;
	}
	@Override
	public String getName() {
		return name;
	}
	
	public int getStudentNumber() {
		return studentNumber;
	}
	public int getPin() {
		return pin;
	}
	
	public int getTuitionBill() {
		return tuitionBill;
	}
	public int getScholarshipAid() {
		return scholarshipAid;
	}
	public int getPeriodOfPaymentPlan() {
		return periodOfPaymentPlan;
	}
	public int getMaxCredits() {
		return maxCredits;
	}
	public boolean isGraduate() {
		return graduate == "True";
	}
	public ArrayList<Pair<Course, String>> getGrades() {
		return grades;
	}
	
	public int getCredits() {
		return credits;
	}
	public void setCredits(int credits) {
		this.credits = credits;
	}
	public ArrayList<Course> getSchedule() {
		return schedule;
	}
	
	public int getNumCourses() {
		return this.getSchedule().size();
	}	
	public boolean addCourse(Course course) {
		return this.getSchedule().add(course);
	}	
	public boolean removeCourse(Course course) {
		return this.getSchedule().remove(course);
	}	
	public boolean isTuitionPaid() {
		return tuitionBill == 0;
	}	
	public boolean hasScholarshipAid() {
		return scholarshipAid != 0;
	}	
	public boolean hasPeriodOfPaymentPlan() {
		return periodOfPaymentPlan != 1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + credits;
		result = prime * result + id;
		result = prime * result + ((graduate == null) ? 0 : graduate.hashCode());
		result = prime * result + maxCredits;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + studentNumber;
		result = prime * result + pin;
		result = prime * result + periodOfPaymentPlan;
		result = prime * result + scholarshipAid;
		result = prime * result + tuitionBill;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (credits != other.credits)
			return false;
		if (id != other.id)
			return false;
		if (graduate != other.graduate)
			return false;
		if (maxCredits != other.maxCredits)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (studentNumber != other.studentNumber)
			return false;
		if (pin != other.pin)
			return false;
		if (periodOfPaymentPlan != other.periodOfPaymentPlan)
			return false;
		if (scholarshipAid != other.scholarshipAid)
			return false;
		if (tuitionBill != other.tuitionBill)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", studentNumber=" + studentNumber +  ", pin=" + pin 
				+ ", tuitionBill=" + tuitionBill + ", scholarshipAid=" + scholarshipAid + ", periodOfPaymentPlan=" 
				+ periodOfPaymentPlan + ", maxCredits=" + maxCredits + ", isGraduate=" + graduate + ", credits=" + credits + "]";
	}
	
	//just for testing
	public void setGrades(ArrayList<Pair<Course, String>> grades) {this.grades = grades;}
}