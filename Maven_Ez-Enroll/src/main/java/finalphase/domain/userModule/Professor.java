package finalphase.domain.userModule;

import java.util.ArrayList;

import finalphase.domain.Searchable;
import finalphase.domain.courseModule.Course;

public class Professor implements Searchable {

	
	private int id;
	private String name;
	
	private String username;
	private String password;
	
	private ArrayList<Course> schedule;

	public Professor() {}
	public Professor(int id, String name, String username, String password, ArrayList<Course> schedule) {
		this.id = id;
		this.name = name;
		
		this.username = username;
		this.password = password;
		this.schedule = schedule;
	}

	@Override
	public int getId() {
		return id;
	}
	@Override
	public String getName() {
		return name;
	}
	
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public ArrayList<Course> getSchedule() {
		return schedule;
	}

	public int getNumCourses() {
		return this.getSchedule().size();
	}
	
	public boolean modifyCourseDescription(Course course, String description) {
		if(isValidProfessor(this, course)) {
			course.setDescription(description);
			return true;
		}
		return false;
	}
	
	private static boolean isValidProfessor(Professor p, Course c) {
		return p != null && c != null && c.getProfessor() == p;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Professor other = (Professor) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Professor [id=" + id + ", name=" + name + ", username=" + username + ", password=" + password + "]";
	}
	
	//just for testing
	public void setSchedule(ArrayList<Course> schedule) {this.schedule = schedule;}
}