package finalphase.domain;

public interface Searchable {
	public int getId();
	public String getName();
}