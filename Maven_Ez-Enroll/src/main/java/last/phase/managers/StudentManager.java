package last.phase.managers;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import finalphase.domain.courseModule.Course;
import finalphase.domain.userModule.Student;

import java.util.ArrayList;
import java.util.LinkedList;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;


@Path("/student")

public class StudentManager {

	public static LinkedList<Student> sList = new LinkedList<>();

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addStudent(Student s) {
		for (int i = 0; i < sList.size(); i++) {
			Student one = sList.get(i);
			if (one.getId() == s.getId()) {
				return Response.status(Response.Status.CONFLICT).build();
			}
		}
		sList.add(s);
		return Response.status(201).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Student[] allStudents() {
		if (sList.isEmpty())
			throw new NotFoundException(new JsonError("Error", "Student list not found"));
		Student[] sarr = new Student[sList.size()];
		return sList.toArray(sarr);

	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Student getStudent(@PathParam("id") int id) {
		for (Student student : sList) {
			if (student.getId() == id) {
				return student;
			}
		}
		throw new NotFoundException(new JsonError("Error", "Student " + id + " not found"));
	}
	
	@GET
	@Path("{id}/courses")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Course> getEnrolledCourses(@PathParam("id") int id) {
		for (Student student : sList) {
			if (student.getId() == id) {
				return student.getSchedule();
			}
		}
		throw new NotFoundException(new JsonError("Error", "Student " + id + " not found"));
	}
	
	//has duplicates
	@PUT
	@Path("{id}/enroll")
	@Produces(MediaType.APPLICATION_JSON)
	public Response enroll(@PathParam("id") long id, Course c) {
		for (Student s : sList) {
			if (s.getId() == id) {
				s.addCourse(c);
				return Response.status(201).build();
			}
		}
		throw new NotFoundException(new JsonError("Error", "Student " + id + " not found"));
	}
	
	@PUT
	@Path("{id}/drop")
	@Produces(MediaType.APPLICATION_JSON)
	public Response drop(@PathParam("id") long id, Course c) {
		for (Student s : sList) {
			if (s.getId() == id) {
//				ArrayList<Course> t = new ArrayList<Course>();
//				if(s.getSchedule() != null) {
//					t = s.getSchedule();
//				}
//				t.remove(c);
//				s.getSchedule(t);
				s.removeCourse(c);
				return Response.status(201).build();
			}
		}
		throw new NotFoundException(new JsonError("Error", "Student " + id + " not found"));
	}

	@DELETE
	@Path("{id}/delete")
	public Response deleteStudent(@PathParam("id") long id) {
		for (Student s : sList) {
			if (s.getId() == id) {
				sList.remove(s);
				return Response.status(Response.Status.OK).build();
			}
		}
		throw new NotFoundException(new JsonError("Error", "Student " + id + " not found"));
	}
}
