package last.phase.managers;

import javax.ws.rs.NotFoundException;

public class JsonError extends NotFoundException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String type;
	private String message;

	public JsonError(String type, String message) {
		this.type = type;
		this.message = message;
	}

	public String getType() {
		return this.type;
	}

	public String getMessage() {
		return this.message;
	}
}


